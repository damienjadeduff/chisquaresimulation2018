from __future__ import division
import random
from matplotlib import pyplot
from numpy import random
import numpy as np
from collections import defaultdict

nsamples=10000
nbins=40

def rolldie(numsides):

    return random.randint(1,numsides)


def rollNDie(numsides,numthrows):

    freq = defaultdict(int)
    for throw in range(0,numthrows):
        res = rolldie(numsides)
        freq[res] += 1

    return freq


def calc_chisquare_statistic_on_fair_die(result,numsides,numthrows):
    expected=dict()
    for side in range(1,numsides+1):
        expected[side]=numthrows/numsides

    chisquare = 0
    for num in range(1,numsides+1):
        chisquare += (result[num]-expected[num])**2/expected[num]

    return chisquare


def empiricalN(numsides,numthrows):

    results = []

    for rolnum in range(0,nsamples):
        res = rollNDie(numsides,numthrows)
        chis = calc_chisquare_statistic_on_fair_die(res,numsides,numthrows)
        results.append(chis)

    pyplot.hist(results,bins=nbins)
    pyplot.ylabel("Frequency")
    pyplot.xlabel("X^2 Statistic")
    pyplot.title("Histogram of Chi Square statistic for "+str(numthrows)+" rolls of "+str(numsides)+"-sided die against expected")
    pyplot.show()
#    print("Mean of empirical "+str(numthrows)+" rolls of "+str(numsides)+"-sided dice chi square is "+str(np.average(results)))
    print("95th percentile of empirical "+str(numthrows)+" rolls of "+str(numsides)+"-sided dice chi square is "+str(np.percentile(results,95)))



def theoretical(dof):

    results = random.chisquare(dof,nsamples)
    pyplot.hist(results,bins=nbins)
    pyplot.ylabel("Frequency")
    pyplot.xlabel("X^2 Statistic")
    pyplot.title("Histogram of theoretical Chi Square statistic "+str(dof)+" DOF")
    pyplot.show()
    print("95th percentile of theoretical chi square with DOF "+str(dof)+" is "+str(np.percentile(results,95)))


theoretical(5)

empiricalN(6,200)
empiricalN(6,5000)
#
#for i in range(0,100):
#    x = empiricalN(6,7)
